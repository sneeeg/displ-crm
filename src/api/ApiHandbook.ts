import axios, { AxiosResponse } from "axios";
import IEntryResponse from "@/struct/handbook/IEntryResponse";

class ApiHandbook {
    public static async GetList<T>(session_uuid: string, entryName: string, from: string | undefined = undefined, uuid: string | undefined = undefined): Promise<IEntryResponse<T>[] | boolean> {
        try {
            const result: AxiosResponse<{ response: T[], references: any }> = await axios.get(`/api/admin/${entryName}/get-list` + (from != undefined ? `/from-${from}?${from}_uuid=` + uuid : ""), {
                headers: {
                    "x-tenant": "null",
                    "x-session-token": session_uuid
                }
            });

            const response: IEntryResponse<T>[] = [];
            for (const entry of result.data.response) {
                const entryResponse: IEntryResponse<T> = {
                    entry: entry,
                    refs: {}
                };
                response.push(entryResponse);
                for (const key in result.data.references) {
                    for (const ref of result.data.references[key]) {
                        if ((entry as any)[key + "_uuid"] == ref.uuid) {
                            entryResponse.refs[key] = ref;
                        }
                    }
                }
            }
            return response;
        } catch (e) {
            return false;
        }
    }

    public static async GetEntry<T>(session_uuid: string, entryName: string, uuid: string): Promise<IEntryResponse<T> | null> {
        try {
            const result: AxiosResponse<{ response: T, references: any }> = await axios.get(`/api/admin/${entryName}/get?uuid=${uuid}`, {
                headers: {
                    "x-tenant": "null",
                    "x-session-token": session_uuid
                }
            });
            const refs: any = {};
            for (const key in result.data.references) {
                if (result.data.references[key].length == 0) {
                    refs[key] = null;
                } else {
                    refs[key] = result.data.references[key][0];
                }

            }

            return {
                refs: refs,
                entry: result.data.response
            };
        } catch (e) {
            return null;
        }
    }

    public static async CreateTariff(session_uuid: string, price: string, type: string, model_uuid: string, name: string): Promise<boolean | undefined> {
        try {
            const result: AxiosResponse<{ status: boolean }> = await axios.post(`/api/admin/tariff/create`, {
                price: Number(price),
                type: Number(type),
                model_uuid: model_uuid,
                name: name,
                status: 1
            }, {
                headers: {
                    "x-tenant": "null",
                    "x-session-token": session_uuid
                }
            });

            result.data.status;
        } catch (e) {
            return undefined;
        }
    }

    public static async ChangeStatusTest(session_uuid: string, status: number, uuid: string): Promise<boolean | undefined> {
        try {
            const result: AxiosResponse<{ response: boolean }> = await axios.post(`/api/admin/test/update`, {
                status: status,
                uuid: uuid
            }, {
                headers: {
                    "x-tenant": "null",
                    "x-session-token": session_uuid
                }
            });

            return result.data.response;
        } catch (e) {
            console.error(e);
            return undefined;
        }
    }

    public static async ModelUpdate(session_uuid: string, status: boolean, uuid: string): Promise<boolean | undefined> {
        try {
            const result: AxiosResponse<{ response: boolean }> = await axios.post(`/api/admin/model/update`, {
                status: status == true ? 1 : 0,
                uuid: uuid
            }, {
                headers: {
                    "x-tenant": "null",
                    "x-session-token": session_uuid
                }
            });

            return result.data.response;
        } catch (e) {
            console.error(e);
            return undefined;
        }
    }

    public static async TariffDeactivate(session_uuid: string, status: boolean, uuid: string): Promise<boolean | undefined> {
        try {
            const result: AxiosResponse<{ response: boolean }> = await axios.post(`/api/admin/tariff/update`, {
                status: status == true ? 2 : 0,
                uuid: uuid
            }, {
                headers: {
                    "x-tenant": "null",
                    "x-session-token": session_uuid
                }
            });

            return result.data.response;
        } catch (e) {
            console.error(e);
            return undefined;
        }
    }

    public static async TariffUpdate(session_uuid: string, uuid: string, price: string, type: string, model_uuid: string, name: string, status: number): Promise<boolean | undefined> {
        try {
            const result: AxiosResponse<{ response: boolean }> = await axios.post(`/api/admin/tariff/update`, {
                uuid: uuid,
                price: Number(price),
                type: Number(type),
                model_uuid: model_uuid,
                name: name,
                status: status
            }, {
                headers: {
                    "x-tenant": "null",
                    "x-session-token": session_uuid
                }
            });

            return result.data.response;
        } catch (e) {
            console.error(e);
            return undefined;
        }
    }

    public static async GetIssueComment(session_uuid: string, uuid: string): Promise<string | boolean> {
        try {
            const result: AxiosResponse<{ response: string }> = await axios.get(`/api/admin/device_issue_comment/get?uuid=${uuid}`, {
                headers: {
                    "x-tenant": "null",
                    "x-session-token": session_uuid
                }
            });

            return result.data.response;
        } catch (e) {
            console.error(e);
            return false;
        }
    }

    public static async CreateNewModel(session_uuid: string, name: string, model_id: string): Promise<boolean | undefined> {
        try {
            const result: AxiosResponse<{ response: boolean }> = await axios.post(`/api/admin/model/create`, {
                name: name,
                model_id: model_id,
                status: 1
            }, {
                headers: {
                    "x-tenant": "null",
                    "x-session-token": session_uuid
                }
            });
            return result.data.response;
        } catch (e) {
            console.error(e);
            return undefined;
        }
    }

    public static async UpdateDeviceIssue(session_uuid: string, uuid: string, status: number, company_uuid: string, first_description: string): Promise<boolean | undefined> {
        try {
            const result: AxiosResponse<{ response: boolean }> = await axios.post(`/api/admin/device_issue/update`, {
                uuid: uuid,
                status: status,
                company_uuid: company_uuid,
                first_description: first_description
            }, {
                headers: {
                    "x-tenant": "null",
                    "x-session-token": session_uuid
                }
            });
            return result.data.response;
        } catch (e) {
            console.error(e);
            return undefined;
        }
    }

    public static async UpdateDeviceIssueComment(session_uuid: string, uuid: string, comment: string): Promise<boolean | undefined> {
        try {
            const result: AxiosResponse<{ response: boolean }> = await axios.post(`/api/admin/device_issue_comment/update`, {
                uuid: uuid,
                comment: comment
            }, {
                headers: {
                    "x-tenant": "null",
                    "x-session-token": session_uuid
                }
            });
            return result.data.response;
        } catch (e) {
            console.error(e);
            return undefined;
        }
    }

    public static async CreateIssue(session_uuid: string, device_uuid: string, status: string, first_description: string, company_uuid: string, name: string): Promise<boolean> {
        try {
            const result = await axios.post("/api/admin/device_issue/create", {
                status: status,
                device_uuid: device_uuid,
                name: name,
                first_description: first_description,
                company_uuid: company_uuid
            }, {
                headers: {
                    "x-tenant": "null",
                    "x-session-token": session_uuid
                }
            });
            if ("status" in result.data == false || result.data.status == false) {
                return false;
            }
            return result.data.response;
        } catch (e) {
            return false;
        }

    }

}

export default ApiHandbook;
