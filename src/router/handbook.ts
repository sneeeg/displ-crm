import { RouteConfig } from "vue-router";
import DevicesList from "@/views/Devices/DevicesList.vue"
import RateList from "@/views/Tariff/TariffList.vue"

const routes: Array<RouteConfig> = [
    {
        path: '/devices-list',
        name: 'DevicesList',
        component: DevicesList
    },
    {
        path: '/rate-list',
        name: 'RateList',
        component: RateList
    }
]

export default routes
