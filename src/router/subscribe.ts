import { RouteConfig } from "vue-router";
import SubscribeView from "@/views/Subscribe/SubscribeView.vue"
import SubscribeList from "@/views/Subscribe/SubscribeList.vue"

const routes: Array<RouteConfig> = [
    {
        path: '/subscribe/:subscribe_uuid',
        name: 'SubscribeView',
        component: SubscribeView
    },
    {
        path: '/subscribe-list',
        name: 'SubscribeList',
        component: SubscribeList
    }
]

export default routes
