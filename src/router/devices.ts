import { RouteConfig } from "vue-router";
import DevicesList from "@/views/Tariff/TariffList.vue"

const routes: Array<RouteConfig> = [
    {
        path: '/devices-list',
        name: 'DevicesList',
        component: DevicesList
    }
]

export default routes
