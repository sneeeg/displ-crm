import dayjs from "dayjs";

class UtilDate {
    public static ValidateDate(date: string): string {
        if (date) {
            return dayjs(date).format('DD.MM.YYYY (HH:mm:ss)')
        }
        return ""

    }
    public static CurrentDate(): string {
        return dayjs().format("DD.MM.YYYY (HH:mm:ss)");
    }
}

export default UtilDate
