import { Vue } from "vue-property-decorator";

class UtilApp {
    public static ForceUpdatePage(self: Vue) {
        self.$forceUpdate()
        for (const comp of self.$children) {
            UtilApp.ForceUpdatePage(comp)
        }
    }
}

export default UtilApp
