interface BreadcrumbsItemType {
    "active-class"?: string;
    append?: boolean;
    disabled?: boolean;
    exact?: boolean;
    "exact-active-class"?: string;
    "exact-path"?: boolean;
    href?: string ;
    link?: boolean;
    nuxt?: boolean;
    replace?: boolean;
    ripple?: boolean ;
    tag?: string;
    text?: string;
    target?: string;
    to?: string ;
}

export default BreadcrumbsItemType;
