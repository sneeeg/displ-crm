interface TableHeaderItemType {
    text: string;
    value: string;
    align?: string;
    sortable?: boolean;
    filter?: (value: any) => string | number | boolean
}

export default TableHeaderItemType;
