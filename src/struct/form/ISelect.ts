interface VSelectType {
    // eslint-disable-next-line @typescript-eslint/ban-types
    text: string | number | object;
    // eslint-disable-next-line @typescript-eslint/ban-types
    value: string | number | object;
    disabled?: boolean;
    divider?: boolean;
    header?: string;
}

export default VSelectType
