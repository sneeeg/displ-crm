export interface ICustomerInfo {
    first_name: string;
    last_name: string;
    phone: string;
    email: string;
    uuid: string;
}

export interface IDeviceInfo {
    imei: string;
    uuid: string;
    name: string;
}

export interface ITariffInfo {
    name: string;
    create_at: string;
    status: number;
    first_payment_at: string;
    last_payment_at: string;
    next_payment_at: string;
}
