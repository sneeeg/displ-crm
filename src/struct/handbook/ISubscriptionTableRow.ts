export namespace subscriptionFields {
    export type create_at = Date;
    export type customer_uuid = string;
    export type device_uuid = string;
    export type finish_at = Date;
    export type id = number;
    export type touch_finish_at = string | null;
    export type last_amount = number;
    export type last_payment_at = Date;
    export type status = boolean;
    export type tariff_uuid = string;
    export type uuid = string;
}

export interface ISubscriptionTableRow {
    create_at: subscriptionFields.create_at;
    customer_uuid: subscriptionFields.customer_uuid;
    device_uuid: subscriptionFields.device_uuid;
    finish_at: subscriptionFields.finish_at;
    touch_finish_at: subscriptionFields.touch_finish_at;
    id: subscriptionFields.id;
    last_amount: subscriptionFields.last_amount;
    last_payment_at: subscriptionFields.last_payment_at;
    status: subscriptionFields.status;
    tariff_uuid: subscriptionFields.tariff_uuid;
    uuid: subscriptionFields.uuid;
}
