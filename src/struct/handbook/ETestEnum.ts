export namespace ETestEnum {
    export enum TestStatus {
        Created,
        Pending,
        Approval,
        Decline,
        Cancel
    }
}
