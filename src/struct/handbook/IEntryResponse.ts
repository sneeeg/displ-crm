interface IEntryResponse<T> {
    refs: { [key: string]: any },
    entry: T
}

export default IEntryResponse
