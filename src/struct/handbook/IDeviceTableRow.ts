
export namespace deviceFields {
    export type create_at = Date;
    export type customer_uuid = string;
    export type device_uuid = string;
    export type id = number;
    export type imei = string;
    export type model_uuid = string;
    export type uuid = string;
}

export interface IDeviceTableRow {
    create_at: deviceFields.create_at;
    customer_uuid: deviceFields.customer_uuid;
    device_uuid: deviceFields.device_uuid;
    id: deviceFields.id;
    imei: deviceFields.imei;
    model_uuid: deviceFields.model_uuid;
    uuid: deviceFields.uuid;
    status: boolean;
}
