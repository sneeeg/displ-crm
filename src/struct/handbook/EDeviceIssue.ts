export namespace EDeviceIssue {
    export enum DeviceIssueStatus {
        IssueCreated,
        IssueIsBad,
        IssueSuccess
    }
}
