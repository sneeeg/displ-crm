export namespace payment_historyFields {
    export type amount = number;
    export type create_at = Date;
    export type customer_uuid = string;
    export type finish_at = Date | null;
    export type id = number;
    export type payment_method_uuid = string;
    export type status = boolean;
    export type subscription_uuid = string;
    export type tariff_uuid = string;
    export type uuid = string;
}

export interface IPaymentHistoryTableRow {
    amount: payment_historyFields.amount;
    create_at: payment_historyFields.create_at;
    customer_uuid: payment_historyFields.customer_uuid;
    finish_at: payment_historyFields.finish_at;
    id: payment_historyFields.id;
    payment_method_uuid: payment_historyFields.payment_method_uuid;
    status: payment_historyFields.status;
    subscription_uuid: payment_historyFields.subscription_uuid;
    tariff_uuid: payment_historyFields.tariff_uuid;
    uuid: payment_historyFields.uuid;
}
