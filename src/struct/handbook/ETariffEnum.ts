export namespace ETariffEnum {
    export enum TariffStatus {
        Active,
        Archive,
        Deleted
    }

    export enum TariffType {
        PerMonth,
        PerYear
    }
}
