
export namespace payment_methodFields {
    export type create_at = Date;
    export type customer_uuid = string;
    export type id = number;
    export type method = boolean;
    export type name = string;
    export type status = boolean;
    export type uuid = string;
}

export interface IPaymentMethodTableRow {
    create_at: payment_methodFields.create_at;
    customer_uuid: payment_methodFields.customer_uuid;
    id: payment_methodFields.id;
    method: payment_methodFields.method;
    name: payment_methodFields.name;
    status: payment_methodFields.status;
    uuid: payment_methodFields.uuid;
}
