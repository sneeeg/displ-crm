export namespace tariffFields {
    export type create_at = string;
    export type id = number;
    export type model_uuid = string;
    export type origin_tariff_uuid = string;
    export type name = string;
    export type price = number;
    export type status = boolean;
    export type type = boolean;
    export type uuid = string;
}

export interface ITariffTableRow {
    create_at: tariffFields.create_at;
    id: tariffFields.id;
    model_uuid: tariffFields.model_uuid;
    origin_tariff_uuid: tariffFields.origin_tariff_uuid;
    name: tariffFields.name;
    price: tariffFields.price;
    status: tariffFields.status;
    type: tariffFields.type;
    uuid: tariffFields.uuid;
}
