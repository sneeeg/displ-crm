export namespace modelFields {
    export type create_at = Date;
    export type id = number;
    export type model_id = string;
    export type name = string;
    export type status = boolean;
    export type uuid = string;
}

export interface IModelTableRow {
    create_at: modelFields.create_at;
    id: modelFields.id;
    model_id: modelFields.model_id;
    name: modelFields.name;
    status: modelFields.status;
    uuid: modelFields.uuid;
}
