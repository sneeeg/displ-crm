export namespace customerFields {
    export type create_at = Date;
    export type email = string;
    export type first_name = string;
    export type id = number;
    export type last_name = string;
    export type phone = string;
    export type uuid = string;
}

export interface ICustomerTableRow {
    create_at: customerFields.create_at;
    email: customerFields.email;
    first_name: customerFields.first_name;
    id: customerFields.id;
    last_name: customerFields.last_name;
    phone: customerFields.phone;
    uuid: customerFields.uuid;
}
