export namespace EPaymentStatus {
    export enum PaymentStatus {
        Pending,
        Success,
        Failed,
        Refund,
    }
}
