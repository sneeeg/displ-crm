export namespace ETariffEnum {
    export enum SubscriptionStatus {
        Pending,
        Refund,
        Cancel,
        Active,
        EndTime,
        Finish
    }
}
