import TableHeaderItemType from "@/struct/ui/Table/TableHeaderItemType";
import BreadcrumbsItemType from "@/struct/ui/breadcrumbs/BreadcrumbsItemType";
import { FormGeneratorListInputModelType } from "@/struct/form/FormGenerator/FormGeneratorListInputModelType";
import UtilFormValidation from "@/util/Form/UtilFormValidation";

class DataDevicesList {

    public static readonly Breadcrumbs: BreadcrumbsItemType[] = [
        {
            to: "/",
            text: "Главная"
        },
        {
            to: "/devices-list",
            text: "Список устройств",
            disabled: true
        }
    ];

    public static TableHeaders: TableHeaderItemType[] = [
        {
            text: "Модель",
            value: "entry.name"
        },
        {
            text: "Действия",
            value: "action"
        }
    ];

    public static DeviceCreateModel: FormGeneratorListInputModelType = {
        Name: {
            name: "Производитель",
            value: "",
            default: "",
            type: "text",
            maxlength: 32,
            rules: [...UtilFormValidation.CreateOnMinValueLengthRules(2), ...UtilFormValidation.CreateOnMaxValueLengthRules(32)]
        },
        Model: {
            name: "Модель",
            value: "",
            default: "",
            type: "text",
            maxlength: 32,
            rules: [...UtilFormValidation.CreateOnMinValueLengthRules(2), ...UtilFormValidation.CreateOnMaxValueLengthRules(32)]
        }
    };

}

export default DataDevicesList;
