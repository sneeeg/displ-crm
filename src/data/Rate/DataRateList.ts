import TableHeaderItemType from "@/struct/ui/Table/TableHeaderItemType";
import BreadcrumbsItemType from "@/struct/ui/breadcrumbs/BreadcrumbsItemType";
import { FormGeneratorListInputModelType } from "@/struct/form/FormGenerator/FormGeneratorListInputModelType";
import UtilFormValidation from "@/util/Form/UtilFormValidation";
import { ETariffEnum } from "@/struct/handbook/ETariffEnum";

class DataRateList {

    public static readonly Breadcrumbs: BreadcrumbsItemType[] = [
        {
            to: "/",
            text: "Главная"
        },
        {
            to: "/devices-list",
            text: "Список тарифов",
            disabled: true
        }
    ];

    public static TableHeaders: TableHeaderItemType[] = [
        {
            text: "Название",
            value: "entry.name"
        },
        {
            text: "Модель",
            value: "refs.model.name"
        },
        {
            text: "Тип тарифа",
            value: "entry.type"
        },
        {
            text: "Цена",
            value: "entry.price"
        },
        {
            text: "Действия",
            value: "action"
        }
    ];

    public static RateCreateModel: FormGeneratorListInputModelType = {
        Name: {
            name: "Название",
            placeholder: "Введите название тарифа",
            value: "",
            default: "",
            type: "text",
            maxlength: 32,
            rules: [...UtilFormValidation.CreateOnMinValueLengthRules(2), ...UtilFormValidation.CreateOnMaxValueLengthRules(32)]
        },
        Model: {
            name: "Модель устройства",
            placeholder: "Выберите модель устройства",
            value: "",
            default: "",
            type: "select",
            select: [],
            maxlength: 32,
            rules: [...UtilFormValidation.CreateOnMinValueLengthRules(2)]
        },
        TariffType: {
            name: "Тип тарифа",
            placeholder: "Выберите тип тарифа",
            value: "",
            default: "",
            type: "select",
            select: [
                {
                    label: "Месяц",
                    value: ETariffEnum.TariffType.PerMonth
                },
                {
                    label: "Год",
                    value: ETariffEnum.TariffType.PerYear
                }],
            maxlength: 32,
            rules: []
        },
        Price: {
            name: "Цена",
            placeholder: "Введите цену в рублях",
            value: "",
            default: "",
            type: "text",
            maxlength: 32,
            rules: [...UtilFormValidation.CreateOnMinValueLengthRules(2), ...UtilFormValidation.CreateOnMaxValueLengthRules(32)]
        }
    };
    public static RateEditModel: FormGeneratorListInputModelType = {
        Name: {
            name: "Название",
            placeholder: "Введите название тарифа",
            value: "",
            default: "",
            type: "text",
            maxlength: 32,
            rules: [...UtilFormValidation.CreateOnMinValueLengthRules(2), ...UtilFormValidation.CreateOnMaxValueLengthRules(32)]
        },
        Model: {
            name: "Модель устройства",
            placeholder: "Выберите модель устройства",
            value: "",
            default: "",
            type: "select",
            select: [],
            maxlength: 32,
            rules: [...UtilFormValidation.CreateOnMinValueLengthRules(2)]
        },
        TariffType: {
            name: "Тип тарифа",
            placeholder: "Выберите тип тарифа",
            value: "",
            default: "",
            type: "select",
            select: [
                {
                    label: "Месяц",
                    value: ETariffEnum.TariffType.PerMonth
                },
                {
                    label: "Год",
                    value: ETariffEnum.TariffType.PerYear
                }],
            maxlength: 32,
            rules: []
        },
        Status: {
            name: "Статус",
            placeholder: "",
            value: "",
            default: "",
            type: "select",
            select: [
                {
                    label: "Активный",
                    value: ETariffEnum.TariffStatus.Active
                },
                {
                    label: "Не активный",
                    value: ETariffEnum.TariffStatus.Deleted
                }],
            maxlength: 32,
            rules: []
        },
        Price: {
            name: "Цена",
            placeholder: "Введите цену в рублях",
            value: "",
            default: "",
            type: "text",
            maxlength: 32,
            rules: []
        }
    };

}

export default DataRateList;
