import BreadcrumbsItemType from "@/struct/ui/breadcrumbs/BreadcrumbsItemType";
import UtilFormValidation from "@/util/Form/UtilFormValidation";
import { FormGeneratorListInputModelType } from "@/struct/form/FormGenerator/FormGeneratorListInputModelType";
import LeftMenuItemType from "@/struct/ui/Menu/LeftMenuItemType";
import ECompanyEnum from "@/struct/company/ECompanyEnum";

class DataEditCompany {

    public static DefaultKeyFromLeftMenu: string = "company-info";


    public static readonly Breadcrumbs: BreadcrumbsItemType[] = [
        {
            to: "/",
            text: "Главная"
        },
        {
            to: "/admin/companies/",
            text: "Администрирование сервисов"
        },
        {
            disabled: true,
            to: "/admin/edit-company/",
            text: "Редактирование сервиса"
        }
    ];

    public static CompanyInfoModel: FormGeneratorListInputModelType = {
        Name: {
            name: "Название сервиса",
            value: "",
            default: "",
            type: "text",
            maxlength: 128,
            rules: [...UtilFormValidation.CreateOnMinValueLengthRules(2), ...UtilFormValidation.CreateOnMaxValueLengthRules(128)]
        },
        Phone: {
            name: "Телефон",
            value: "",
            default: "",
            type: "text",
            maxlength: 19,
            rules: [...UtilFormValidation.CreateOnMaxValueLengthRules(19)]
        },
        City: {
            name: "Город",
            value: "",
            default: "",
            type: "text",
            maxlength: 128,
            rules: [...UtilFormValidation.CreateOnMaxValueLengthRules(128)]
        },
        Address: {
            name: "Адрес сервиса",
            value: "",
            default: "",
            type: "text",
            maxlength: 128,
            rules: [...UtilFormValidation.CreateOnMaxValueLengthRules(128)]
        },
        Address2: {
            name: "Дополнительный адрес",
            value: "",
            default: "",
            type: "text",
            maxlength: 128,
            rules: [...UtilFormValidation.CreateOnMaxValueLengthRules(128)]
        },
        Postcode: {
            name: "Почтовый индекс",
            value: "",
            default: "",
            type: "text",
            maxlength: 128,
            rules: [...UtilFormValidation.CreateOnMaxValueLengthRules(128)]
        },
        Worktime: {
            name: "График работы",
            value: "",
            default: "",
            type: "text",
            maxlength: 128,
            rules: [...UtilFormValidation.CreateOnMaxValueLengthRules(128)]
        }
    };


    public static readonly LeftMenuItems: LeftMenuItemType[] = [
        {
            icon: "fas fa-building",
            label: "Данные о компании",
            key: DataEditCompany.DefaultKeyFromLeftMenu
        },
        {
            icon: "fas fa-trash",
            label: "Удалить компанию",
            key: "delete",
        },
    ];
}

export default DataEditCompany;
