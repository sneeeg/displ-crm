import BreadcrumbsItemType from "@/struct/ui/breadcrumbs/BreadcrumbsItemType";
import TableHeaderItemType from "@/struct/ui/Table/TableHeaderItemType";


export default class DataUser {

    public static Breadcrumbs: BreadcrumbsItemType[] = [
        {
            to: "/",
            text: "Главная",
        },
        {
            to: "/admin/users",
            text: "Администрирование сервисов",
            disabled: true
        },
    ];

    public static TableHeaders: TableHeaderItemType[] = [
        {
            text: "Название сервиса",
            value: "name"
        },
        {
            text: "Адрес",
            value: "addr"
        },
        {
            text: "Телефон",
            value: "phone"
        },
        {
            text: "Действие",
            value: "action"
        }
    ];
}
