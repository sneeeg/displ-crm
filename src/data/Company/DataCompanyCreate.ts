import BreadcrumbsItemType from "@/struct/ui/breadcrumbs/BreadcrumbsItemType";
import UtilFormValidation from "@/util/Form/UtilFormValidation";
import { FormGeneratorListInputModelType } from "@/struct/form/FormGenerator/FormGeneratorListInputModelType";

class DataCompanyCreate {
    public static DefaultKeyForValidate: boolean = true;

    public static readonly Breadcrumbs: BreadcrumbsItemType[] = [
        {
            to: "/",
            text: "Главная"
        },
        {
            to: "/company/list",
            text: "Сервисы"
        },
        {
            disabled: true,
            to: "/company/create/",
            text: "Создание нового сервиса"
        }
    ];

    public static CompanyInfoModel: FormGeneratorListInputModelType = {
        Name: {
            name: "Название сервиса",
            value: "",
            default: "",
            type: "text",
            maxlength: 128,
            rules: [...UtilFormValidation.CreateOnMinValueLengthRules(2), ...UtilFormValidation.CreateOnMaxValueLengthRules(128)]
        },
        Phone: {
            name: "Телефон",
            value: "",
            default: "",
            type: "text",
            maxlength: 19,
            rules: [...UtilFormValidation.CreateOnMaxValueLengthRules(19)]
        },
        City: {
            name: "Город",
            value: "",
            default: "",
            type: "text",
            maxlength: 128,
            rules: [...UtilFormValidation.CreateOnMaxValueLengthRules(128)]
        },
        Address: {
            name: "Адрес сервиса",
            value: "",
            default: "",
            type: "text",
            maxlength: 128,
            rules: [...UtilFormValidation.CreateOnMaxValueLengthRules(128)]
        },
        Address2: {
            name: "Дополнительный адрес",
            value: "",
            default: "",
            type: "text",
            maxlength: 128,
            rules: [...UtilFormValidation.CreateOnMaxValueLengthRules(128)]
        },
        Postcode: {
            name: "Почтовый индекс",
            value: "",
            default: "",
            type: "text",
            maxlength: 128,
            rules: [...UtilFormValidation.CreateOnMaxValueLengthRules(128)]
        },
        Worktime: {
            name: "График работы",
            value: "",
            default: "",
            type: "text",
            maxlength: 128,
            rules: [...UtilFormValidation.CreateOnMaxValueLengthRules(128)]
        }

    };

}

export default DataCompanyCreate;
