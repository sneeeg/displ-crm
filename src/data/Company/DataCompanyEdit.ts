import BreadcrumbsItemType from "@/struct/ui/breadcrumbs/BreadcrumbsItemType";
import UtilFormValidation from "@/util/Form/UtilFormValidation";
import CompanyTableRowType from "@/struct/company/CompanyTableRowType";
import { FormGeneratorListInputModelType } from "@/struct/form/FormGenerator/FormGeneratorListInputModelType";

class DataCompanyEdit {
    public static GetBreadcrumbs(currentCompany: CompanyTableRowType | undefined): BreadcrumbsItemType[] {
        return [
            {
                to: "/",
                text: "Главная"
            },
            {
                to: "/company/list",
                text: "Сервисы"
            },
            {
                to: "/company/dashboard/" + currentCompany?.uuid,
                text: "(" + currentCompany?.id + ") " + currentCompany?.name
            },
            {
                disabled: true,
                to: "/company/edit/" + currentCompany?.uuid + "",
                text: "Редактирование"
            }
        ];
    }

    public static CompanyInfoModel: FormGeneratorListInputModelType = {
        Name: {
            name: "Название сервиса",
            value: "",
            default: "",
            type: "text",
            maxlength: 128,
            rules: [...UtilFormValidation.CreateOnMinValueLengthRules(2), ...UtilFormValidation.CreateOnMaxValueLengthRules(128)],
        },
        Phone: {
            name: "Телефон",
            value: "",
            default: "",
            type: "text",
            maxlength: 13,
            rules: [...UtilFormValidation.CreateOnMaxValueLengthRules(13)],
        },
        Address: {
            name: "Адрес сервиса",
            message: "Фактический адрес, на который будет отправляться почта.",
            value: "",
            default: "",
            type: "text",
            maxlength: 128,
            rules: [...UtilFormValidation.CreateOnMaxValueLengthRules(128)],
        },
    };
}

export default DataCompanyEdit;
