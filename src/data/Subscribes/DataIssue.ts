import { FormGeneratorListInputModelType } from "@/struct/form/FormGenerator/FormGeneratorListInputModelType";
import UtilFormValidation from "@/util/Form/UtilFormValidation";
import { ETariffEnum } from "@/struct/handbook/ETariffEnum";
import { EDeviceIssue } from "@/struct/handbook/EDeviceIssue";

class DataIssue {
    public static DataIssueDialog: FormGeneratorListInputModelType = {
        Status: {
            name: "Статус",
            placeholder: "Выберите статус обращения",
            value: "",
            default: "",
            type: "select",
            select: [
                {
                    label: "Заявка",
                    value: EDeviceIssue.DeviceIssueStatus.IssueCreated
                },
                {
                    label: "Отказ",
                    value: EDeviceIssue.DeviceIssueStatus.IssueIsBad
                },
                {
                    label: "Произведена замена",
                    value: EDeviceIssue.DeviceIssueStatus.IssueSuccess
                }
            ],
            maxlength: 64,
            rules: []
        },
        ServiceProcess: {
            name: "Сервис обработки заявки",
            placeholder: "Выберите сервис обработки заявки",
            value: "",
            default: "",
            select: [],
            type: "select",
            maxlength: 32,
            rules: []
        },
        Description: {
            name: "Комментарий",
            placeholder: "",
            value: "",
            default: "",
            type: "textarea",
            maxlength: 200,
            rules: []
        }
    };
    public static DataCreateIssue: FormGeneratorListInputModelType = {
        Status: {
            name: "Статус",
            placeholder: "Выберите статус обращения",
            value: "",
            default: "",
            type: "select",
            select: [
                {
                    label: "Заявка",
                    value: EDeviceIssue.DeviceIssueStatus.IssueCreated
                },
                {
                    label: "Отказ",
                    value: EDeviceIssue.DeviceIssueStatus.IssueIsBad
                },
                {
                    label: "Произведена замена",
                    value: EDeviceIssue.DeviceIssueStatus.IssueSuccess
                }
            ],
            maxlength: 64,
            rules: []
        },
        ServiceProcess: {
            name: "Сервис обработки заявки",
            placeholder: "Выберите сервис обработки заявки",
            value: "",
            default: "",
            select: [],
            type: "select",
            maxlength: 32,
            rules: []
        },
        Description: {
            name: "Комментарий",
            placeholder: "",
            value: "",
            default: "",
            type: "textarea",
            maxlength: 200,
            rules: []
        }
    };

}

export default DataIssue;
