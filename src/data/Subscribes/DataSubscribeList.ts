import BreadcrumbsItemType from "@/struct/ui/breadcrumbs/BreadcrumbsItemType";
import TableHeaderItemType from "@/struct/ui/Table/TableHeaderItemType";


class DataSubscribeList {
    public static readonly Breadcrumbs: BreadcrumbsItemType[] = [
        {
            to: "/",
            text: "Главная"
        },
        {
            to: "/subscribes",
            text: "Список подписок",
            disabled: true
        }
    ];

    public static TableHeaders: TableHeaderItemType[] = [
        {
            text: "№ Подписки",
            value: "entry.id",
        },
        {
            text: "Дата",
            value: "entry.create_at"
        },
        {
            text: "MSISDN",
            value: "refs.customer.phone"
        },
        {
            text: "Тариф",
            value: "refs.tariff.name"
        },
        {
            text: "Устройство",
            value: "refs.model.name"
        },
        {
            text: "IMEI",
            value: "refs.device.imei"
        },
        {
            text: "Статус подписки",
            value: "entry.status",
        },
        {
            text: "Статус теста экрана",
            value: "refs.test.status"
        }


    ];
}

export default DataSubscribeList;
