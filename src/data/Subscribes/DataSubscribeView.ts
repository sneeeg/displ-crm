import BreadcrumbsItemType from "@/struct/ui/breadcrumbs/BreadcrumbsItemType";
import TableHeaderItemType from "@/struct/ui/Table/TableHeaderItemType";
import { ETestEnum } from "@/struct/handbook/ETestEnum";
import TestStatus = ETestEnum.TestStatus;
import VSelectType from "@/struct/form/ISelect";

class DataSubscribeView {

    public static readonly Breadcrumbs: BreadcrumbsItemType[] = [
        {
            to: "/",
            text: "Главная",
        },
        {
            to: "/subscribe-list",
            text: "Подписки",
        },
        {
            to: "/subscribes",
            text: "+754355435435",
            disabled: true,
        },
    ];

    public static TableHeadersTest: TableHeaderItemType[] = [
        {
            text: 'Дата теста',
            value: 'entry.create_at'
        },
        {
            text: 'Фото экрана',
            value: 'entry.photo_uuids'
        },
        {
            text: 'Статус',
            value: 'entry.status'
        },
        {
            text: 'Модератор',
            value: 'entry.approval_user_uuid'
        },
    ]

    public static TableHeadersFinance: TableHeaderItemType[] = [
        {
            text: 'Дата платежа',
            value: 'entry.create_at'
        },
        {
            text: 'Карта',
            value: 'refs.payment_method.name'
        },
        {
            text: 'Сумма',
            value: 'entry.amount'
        },
        {
            text: 'Статус',
            value: 'entry.status'
        },
    ]

    public static TableHeadersInsurance: TableHeaderItemType[] = [
        {
            text: 'Дата обращения',
            value: 'entry.create_at'
        },
        {
            text: 'Статус',
            value: 'entry.status'
        },
    ]

    public static ChangeStatusOptions: VSelectType[] = [
        {
            text: 'В обработке',
            value: TestStatus.Pending
        },
        {
            text: 'Пройден',
            value: TestStatus.Approval
        },
        {
            text: 'Повторный тест',
            value: TestStatus.Decline
        },
        {
            text: 'Создан',
            value: TestStatus.Created
        },
    ]

}

export default DataSubscribeView
